defmodule Docinho.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  def start(_type, _args) do
    children = [
      { Docinho.Robot, [] },
      telegram_webhook
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: Docinho.Supervisor]
    Supervisor.start_link(children, opts)
  end

  def telegram_webhook do
    %{
      type: :supervisor,
      id: Hedwig.Adapters.Telegram.Webhook,
      start: {Hedwig.Adapters.Telegram.Webhook, :start_link, []}
    }
  end
end
